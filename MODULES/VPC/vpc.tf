provider "aws" {
  version = "~> 2.0"
  region  = "eu-west-1"
}



resource "aws_vpc" "vpc-laboratorio" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "dedicated"

  tags = {
    Name = "vpc-laboratorio"
    Project = "Laboratorio Terraform"
    Environment = "Developed"
  }
}

resource "aws_subnet" "sbnet-laboratorio" {
  vpc_id     = "${aws_vpc.vpc-laboratorio.id}"
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "sbnet-laboratorio"
    Project = "Laboratorio Terraform"
    Environment = "Developed"
  }
}